import os
import time

import datetime
import serial
import txt2wav
import sentence


ch_list = ['919', '4370', '4371', '4379', '4375']
cur_dir = os.path.dirname(os.path.realpath(__file__))


voice = 'Heather'           # English
if sentence.voice.lower() == 'ada':
    voice = 'Ada'           # Dutch


class SIM900Manager:
    ser = None
    timeout = 3
    baudrate = 9600
    cbs = None
    network_name = None

    def __init__(self, port='/dev/ttyAMA0', baudrate=9600, timeout=3):
        self.ser = serial.Serial(port=port, baudrate=baudrate, timeout=timeout)
        self.timeout = timeout
        self.baudrate = baudrate

    def initialize(self):
        print "-- Initializing..."

        self.send_cmd('AT')  # Send 'AT' command to check modem
        rcv = self.read_data()
        if 'OK' in rcv:
            print "Discovering modem: ", rcv
        else:
            print "Failed to discover modem, received val: ", rcv
            return False
        time.sleep(0.1)

        # Factory Reset
        self.send_cmd('AT+CFUN=1,1')
        rcv = self.read_data()
        if 'OK' in rcv:
            print "Factory reset: ", rcv
        else:
            print "Failed to perform factory reset, received val: ", rcv
            return False
        time.sleep(15)

        self.send_cmd('ATE0')  # Disable the Echo
        rcv = self.read_data()
        if 'OK' in rcv:
            print "Disable Echo: ", rcv
        else:
            print "Failed to disable Echo, received val: ", rcv
            return False
        time.sleep(0.1)

        if not self.check_sim_card():
            return False

        # Check network connection
        self.send_cmd('AT+COPS?')
        resp = self.read_data()
        try:
            self.network_name = resp.split('"')[1]
        except IndexError:
            print 'Failed to connect to the network, received val: ', resp

        if self.network_name is not None:
            print 'Found network: ', self.network_name
            txt2wav.text_to_wav(sentence.msg['connected'][voice] + self.network_name + '.', False, 'output.wav', voice)
            os.system('aplay ' + cur_dir + '/output.wav -D sysdefault:CARD=0')

            # Set SIM900 to automatic network selection mode
            self.send_cmd('AT+COPS=0')
            rcv = self.read_data()
            if 'OK' in rcv:
                print "Setting Automatic Network Selection mode: ", rcv
            else:
                print "Failed to set Automatic Network Selection mode, received val: ", rcv

            self.send_cmd('AT+CMGF=1')  # Select Message format as Text mode
            rcv = self.read_data()
            if 'OK' in rcv:
                print "Setting Text mode: ", rcv
            else:
                print "Failed to set TEXT mode, received val: ", rcv
                return False
            time.sleep(0.1)

            # Enable network registration unsolicited result code.
            self.send_cmd('AT+CREG=1')
            rcv = self.read_data()
            if 'OK' in rcv:
                print "Enable network registration unsolicited result code: ", rcv
            else:
                print "Failed to enable network registration unsolicited result code, received val: ", rcv
                return False
            time.sleep(0.1)

            # New CBMs are routed directly to the TE using unsolicited result code
            self.send_cmd('AT+CNMI=2,0,2,0,0')
            rcv = self.read_data()
            if 'OK' in rcv:
                print "Setting CBM mode...", rcv
            else:
                print "Failed to set CBM, received val: ", rcv
                return False
            time.sleep(0.1)

            print 'Selecting Cell Broadcast SMS messages...'
            self.send_cmd('AT+CSCB?')
            rcv = self.read_data()
            print 'Response: ', rcv

            for ch_id in ch_list:
                if ch_id not in rcv:
                    self.send_cmd(('AT+CSCB=0,"' + ch_id + '",""'))
                    rcv1 = self.read_data()
                    if 'OK' in rcv1:
                        print "Adding channel ID: ", rcv1
                    else:
                        print "Failed to set channel ID..."
                        return False
            txt2wav.text_to_wav(sentence.msg['listen'][voice], False, 'output.wav', voice)
            os.system('aplay ' + cur_dir + '/output.wav -D sysdefault:CARD=0')
            return True

        else:
            txt2wav.text_to_wav(sentence.msg['failed'][voice], False, 'output.wav', voice)
            os.system('aplay ' + cur_dir + '/output.wav -D sysdefault:CARD=0')
            return False

    def send_cmd(self, cmd):
        # Transmitting AT Commands to the Modem
        # '\r\n' indicates the Enter key
        cmd += '\r\n'
        self.ser.write(cmd)

    def read_data(self):
        """
        We just read data for about 4 sec(timeout value) because it is a bit tricky to parse response of SIM900,
        and because timing is not so important for this project.
        :return:
        """
        s_time = time.time()
        line = ''
        while True:
            if time.time() - s_time > self.timeout:
                break
            try:
                r = self.ser.read()
                if len(r) > 0:      # Update timer if any data is received...
                    s_time = time.time()
                line += r
            except ValueError as e:
                print e
                time.sleep(0.1)
            except serial.SerialException as e:
                print e
                time.sleep(0.1)

        return line.strip()

    def check_sim_card(self):
        self.send_cmd('AT+CPIN?')
        rcv = self.read_data()
        if 'CPIN: READY' in rcv:
            print "Found SIM card: ", rcv
            return True
        else:
            print "SIM card not found, received val: ", rcv
            return False

    # def check_network(self):
    #     """
    #     Check network connection status
    #     When SIM card is not connected, the response should be '+COPS: 0\r\n\r\nOK\r\n'
    #     When it is connected, response should be '+COPS: 0,0,"AT&T"\r\n\r\nOK\r\n'
    #     :return:
    #     """
    #     self.send_cmd('AT+COPS?')
    #     resp = self.read_data()
    #
    #     # Sometimes CB message is in the response of COPS command...
    #     if "+CBM:" in resp:
    #         msg = resp[resp.index("+CBM:"):]
    #         self.parse_cb_message(msg)
    #         return True
    #
    #     try:
    #         buf = resp.split('"')
    #         self.network_name = buf[1]
    #         return True
    #     except IndexError:
    #         print 'Seems like network connection is lost...'
    #         print 'Response is ', resp
    #         if self.network_name is not None:
    #             self.old_network = self.network_name
    #         self.network_name = None
    #         return False

    def set_baudrate(self, baud_rate):
        self.send_cmd('AT+IPR=' + str(baud_rate))
        rcv = self.read_data()
        print rcv

    def get_baudrate(self):
        self.send_cmd('AT+IPR?')
        rcv = self.read_data()
        return rcv

    def run(self):
        """
        Receive CBM message.
        :return:
        """
        print "Waiting for new message..."
        while True:
            rcv = self.read_data()
            if len(rcv) > 0:
                print 'Data received: ', rcv
                print "Date & Time: ", datetime.datetime.now(), '\n'
                if rcv.startswith('+CBM:'):  # check if any data received
                    # print "Received message: ", rcv
                    for txt in rcv.split('+CB'):
                        if len(txt.strip()) > 0:
                            self.parse_cb_message('+CB' + txt.strip())  # Add '+CB' since we used it for splitting...
                elif '+CREG:' in rcv:  # There is a change in the Network registration or a change of the network cell.
                    # Sample response: +CREG: 1\r\n
                    status = rcv[rcv.index('+CREG:') + 7]
                    if status == '1':
                        self.sound_notify(True)
                    else:
                        self.sound_notify(False)

    def parse_cb_message(self, msg):
        """
        Message type:
            +CBM: <sn>,<mid>,<dcs>,<page>,<pages><CR><LF><data>
        Sample message:
            +CBM: 13120,4370,1,1,2\r\n"This is a PLMN test message running on channel 4370 and 4371."
        :param msg:
        :return:
        """
        print " ----- New CB message is arrived ----- "
        print 'Original message: ', msg
        tmp_list = msg.split(',')
        result = dict()
        result['sn'] = tmp_list[0].split(':')[1].strip()
        result['mid'] = tmp_list[1].strip()
        result['dcs'] = tmp_list[2].strip()
        result['page'] = tmp_list[3].strip()
        result['pages'] = tmp_list[4].splitlines()[0].strip()
        buf = tmp_list[4].splitlines()[1]
        result['message'] = msg[msg.index(buf):]
        print 'Parsed result:'
        for item in result.items():
            print item
        self.cbs = result
        self.play_alarm_sound()
        print 'Successfully played sound...'
        time.sleep(3)

    def play_alarm_sound(self):
        """
        Play sound for 3 times
        :return:
        """
        print "Playing alarm sound..."

        try:
            msg = sentence.msg[self.cbs['mid']][voice] + " : " + self.cbs['message'] + '.'
        except KeyError:
            msg = 'Alert received with the following contents: ' + self.cbs['message'] + '.'

        txt2wav.text_to_wav(msg, False, 'output.wav', voice)
        for i in range(3):
            os.system('aplay ' + cur_dir + '/CMAS_alert.wav -D sysdefault:CARD=0')
            os.system('aplay ' + cur_dir + '/output.wav -D sysdefault:CARD=0')

    def sound_notify(self, status):
        """
        Play sound depends on the network status
        :param status: Network connection status. True: just connected, False: just disconnected
        :param self:
        :return:
        """
        if status:
            self.send_cmd('AT+COPS?')
            resp = self.read_data()
            try:
                self.network_name = resp.split('"')[1]
                print 'Connected to the new network: ', self.network_name
                os.system('aplay ' + cur_dir + '/Startup_sound.wav -D sysdefault:CARD=0')
                txt2wav.text_to_wav(sentence.msg['repair'][voice] + self.network_name + '.', False, 'output.wav', voice)
            except IndexError:
                pass
        else:
            os.system('aplay ' + cur_dir + '/Lost_network.wav -D sysdefault:CARD=0')
            txt2wav.text_to_wav(sentence.msg['lost'][voice] + self.network_name, False, 'output.wav', voice)
            self.network_name = None
        play_sound('output.wav')


def play_sound(file_name):
    """
    Play audio file
    :param file_name:
    :return:
    """
    os.system('aplay ' + cur_dir + '/' + file_name + ' -D sysdefault:CARD=0')

if __name__ == '__main__':

    # If you need to use different port, baudrate, timeout, just identify it something like:
    #     sms = SMSReceiver(port='/dev/ttyAMA0', baudrate=115200, timeout=2)

    txt2wav.text_to_wav(sentence.msg['boot'][voice], False, 'output.wav', voice)
    os.system('aplay ' + cur_dir + '/Startup_sound.wav -D sysdefault:CARD=0')
    os.system('aplay ' + cur_dir + '/output.wav -D sysdefault:CARD=0')

    sms = SIM900Manager()
    sms.initialize()
    sms.run()
