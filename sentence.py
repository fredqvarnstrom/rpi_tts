
voice = 'Ada'

msg = {
    'boot': {
        'Heather': 'One to many C MAS Alert Device version 1 is booting.',
        'Ada': "Wan toe mennie's N El Alurt-melder is aan het opstarten!"
    },
    'connected': {
        'Heather': 'Connected to network named: ',
        'Ada': 'Verbonden aan het mobiele netwerk van: '
    },
    'failed': {
        'Heather': 'Failed to connect to network.',
        'Ada': 'Niet gelukt te verbinden aan het mobiele netwerk.'
    },
    'listen': {
        'Heather': 'Listening to C MAS Wireless Emergency Alerts via Cell Broadcast.',
        'Ada': 'Gereed om N El Alurt-meldingen te ontvangen via cell brotcast.'
    },
    'lost': {
        'Heather': 'Network connection lost. Trying to connect to ',
        'Ada': 'Netwerk verbinding verloren. Proberen te verbinden aan het mobiele netwerk van: '
    },
    'repair': {
        'Heather': 'Device online. Connected to network named: ',
        'Ada': 'Weer succesvol verbonden aan het mobiele netwerk van: '
    },
    '4370': {
        'Heather': 'Presidential Alert received with the following contents ',
        'Ada': 'En El Alurt-melding ontvangen-met de volgende inhoud: '
    },
    '4371': {
        'Heather': 'Extreme Alert received with the following contents ',
        'Ada': 'En El Alurt-melding ontvangen-met de volgende inhoud: '
    },
    '919': {
        'Heather': 'Extreme Alert received with the following contents ',
        'Ada': 'En El Alurt-melding ontvangen-met de volgende inhoud: '
    },
    '4379': {
        'Heather': 'Presidential Alert received with the following contents ',
        'Ada': 'En El Alurt-melding ontvangen-met de volgende inhoud: '
    },
    '4375': {
        'Heather': 'Extreme Alert received with the following contents ',
        'Ada': 'En El Alurt-melding ontvangen-met de volgende inhoud: '
    },
}
