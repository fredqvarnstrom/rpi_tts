#!/usr/bin/python

# Copyright (c) 2011 Cereproc Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

#
# This is a simple program to produce audio output using the CereVoice
# Engine API.
#      

#
# Standard imports
#
import os
import sys

# Add CereVoice Engine to the path
sdkdir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
engdir = os.path.join(sdkdir, 'cerevoice_eng', 'pylib')
auddir = os.path.join(sdkdir, 'cerevoice_aud', 'pylib')
sys.path.append(engdir)
sys.path.append(auddir)
                         
#
# Cereproc imports
#
import cerevoice_eng
import cerevoice_aud

# User data class to store information we would like to have
# access to in the callback.
class EngineUserData:
    def __init__(self, wavout, engine, channel, player):
        # If false, audio is being played
        self.wavout = wavout
        # Audio player
        self.player = player
        # These two are optional, they can be used to control
        # cancellation in the callback function.
        self.engine = engine
        self.channel = channel

# Channel event callback function
class CereVoiceEngineCallback:
    def __init__(self, userdata):
        # User-configurable parameter, could be as simple as a file 
        # name for the output, or a richer data structure.
        self.userdata = userdata

    # The callback function must be called 'channel_callback'
    def channel_callback(self, data):
        # Get the audio buffer for this piece of synthesis output
        abuf = cerevoice_eng.data_to_abuf(data)
        # Print transcription information from the audio buffer
        # This information could be used for lip syncing, markers
        # can be used to send data to this application via the 
        # engine.
        for i in range(cerevoice_eng.CPRC_abuf_trans_sz(abuf)):
            trans = cerevoice_eng.CPRC_abuf_get_trans(abuf, i)
            if trans:
                start = cerevoice_eng.CPRC_abuf_trans_start(trans)
                end = cerevoice_eng.CPRC_abuf_trans_end(trans)
                name = cerevoice_eng.CPRC_abuf_trans_name(trans)
                if cerevoice_eng.CPRC_abuf_trans_type(trans) == cerevoice_eng.CPRC_ABUF_TRANS_PHONE:
                    print "INFO: phoneme '%s', start '%s', end '%s'" % (name, start, end) 
                elif cerevoice_eng.CPRC_abuf_trans_type(trans) == cerevoice_eng.CPRC_ABUF_TRANS_WORD:
                    print "INFO: word '%s', start '%s', end '%s'" % (name, start, end) 
                elif cerevoice_eng.CPRC_abuf_trans_type(trans) == cerevoice_eng.CPRC_ABUF_TRANS_MARK:
                    print "INFO: marker '%s', start '%s', end '%s'" % (name, start, end) 
                else:
                    print "WARNING: transcription type '%s' not known" % cerevoice_eng.CPRC_abuf_trans_type(trans)
        # Save the output.  Creates a new file on the first call, 
        # appends on subsequent calls
        if self.userdata.wavout:
            print "INFO: appending to ", self.userdata.wavout
            cerevoice_eng.CPRC_riff_append(abuf, self.userdata.wavout)
        else:
            # Cue the audio for playing
            if self.userdata.player:
                buf = cerevoice_aud.CPRC_sc_audio_short_disposable(cerevoice_eng.CPRC_abuf_wav_data(abuf), 
                                                                   cerevoice_eng.CPRC_abuf_wav_sz(abuf))
                cerevoice_aud.CPRC_sc_audio_cue(self.userdata.player, buf)

        # Example of resetting the channel from within the callback.
        # After a reset the callback will no longer fire, and
        # synthesis will stop.
        #cerevoice_eng.CPRCEN_engine_channel_reset(self.userdata.engine, self.userdata.channel)


def main():
    from optparse import OptionParser

    # Default input/output directory
    cwd = os.getcwd()
    
    # Setup option parsing
    usage="usage: %prog [options] -L licensefile -V voicefile infile1 [infile2...]\nSynthesise an xml/text file to a wave file and transcription, or play back the audio."
    parser = OptionParser(usage=usage)

    parser.add_option("-L", "--licensefile", dest="licensefile",
                      help="CereProc license file")
    parser.add_option("-V", "--voicefile", dest="voicefile",
                      help="Voice file")
    parser.add_option("-o", "--outdir", dest="outdir", default=cwd,
                      help="Output directory, defaults to '%s'" % cwd)
    parser.add_option("-p", "--play", dest="play", action="store_true", default=False,
                      help="Play back the audio instead of saving, defaults to false")
    parser.add_option("-d", "--ondisk", dest="ondisk", action="store_true",
                      default=False, help="Load keeping audio and index data on disk")
    
    opts, args = parser.parse_args()

    # Check correct info supplied
    if len(args) < 1:
        parser.error("at least one input file must be supplied")
    if not opts.voicefile:
        parser.error("a voice file must be supplied")
    if not os.access(opts.voicefile, os.R_OK):
        parser.error("can't access voice file '%s'" % opts.voicefile)
    if not opts.licensefile:
        parser.error("a license file must be supplied")
    if not os.access(opts.licensefile, os.R_OK):
        parser.error("can't access license file '%s'" % opts.licensefile)
    if opts.outdir:
        if not os.access(opts.outdir, os.W_OK):
            parse.error("can't write to output directory output directory '%s'", opts.outdir)

    # Create an engine
    engine = cerevoice_eng.CPRCEN_engine_new()

    # Set the loading mode - all data to RAM or with audio and indexes on disk
    loadmode = cerevoice_eng.CPRC_VOICE_LOAD
    if opts.ondisk:
        loadmode = cerevoice_eng.CPRC_VOICE_LOAD_EMB

    # Load the voice
    ret = cerevoice_eng.CPRCEN_engine_load_voice(engine, opts.licensefile, "", opts.voicefile, cerevoice_eng.CPRC_VOICE_LOAD_EMB)
    if not ret:
        sys.stderr.write("ERROR: could not load the voice, check license integrity\n")
        sys.exit(1)
    info = cerevoice_eng.CPRCEN_engine_get_voice_info(engine, 0, "VOICE_NAME")
    sample_rate = cerevoice_eng.CPRCEN_engine_get_voice_info(engine, 0, "SAMPLE_RATE")
    sys.stderr.write("INFO: voice name is '%s', sample rate '%s'\n" % (info, sample_rate))

    # Process the input files
    for f in args:
        indata = open(f).read()
        # Synthesise to a file
        if opts.play:
            wavout = False
        else:
            wavout = os.path.join(opts.outdir, os.path.basename(os.path.splitext(f)[0])) + ".wav"
            if os.path.isfile(wavout):
                print "INFO: removing previous output file '%s'" % wavout
                os.remove(wavout)
        channel = cerevoice_eng.CPRCEN_engine_open_default_channel(engine)
        freq = int(cerevoice_eng.CPRCEN_channel_get_voice_info(engine, channel, "SAMPLE_RATE"))
        if opts.play:
            player = cerevoice_aud.CPRC_sc_player_new(freq)
            userdata = EngineUserData(wavout, engine, channel, player)
        else:
            userdata = EngineUserData(wavout, engine, channel, False)
        cc = CereVoiceEngineCallback(userdata)
        res = cerevoice_eng.engine_set_callback(engine, channel, cc)
        if res: 
            print "INFO: callback set successfully"
            cerevoice_eng.CPRCEN_engine_channel_speak(engine, channel, indata, len(indata), 1)
            if wavout: print "INFO: wrote wav file '%s'" % wavout
        else:
            sys.stderr.write("ERROR: could not set callback, synthesis data cannot be processed")

        if opts.play:
            while cerevoice_aud.CPRC_sc_audio_busy(player):
                cerevoice_aud.CPRC_sc_sleep_msecs(50)
            cerevoice_aud.CPRC_sc_player_delete(player)
    # Clean up
    cerevoice_eng.CPRCEN_engine_delete(engine)

if __name__ == '__main__':
    main()
