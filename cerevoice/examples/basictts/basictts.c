/* Copyright (c) 2010-2011 Cereproc Ltd. */

/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the */
/* "Software"), to deal in the Software without restriction, including */
/* without limitation the rights to use, copy, modify, merge, publish, */
/* distribute, sublicense, and/or sell copies of the Software, and to */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions: */

/* The above copyright notice and this permission notice shall be */
/* included in all copies or substantial portions of the Software. */

/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE */
/* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION */
/* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION */
/* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cerevoice_eng_simp.h>
#include <cerevoice_aud.h>

#define MAX_FILE_SIZE 262144

void usage(char * name){
    fprintf(stderr, "basictts - a sample TTS program using the CereVoice Engine Simple API.\n");
    fprintf(stderr, "Audio is played using the CereVoice Audio library.\n\n");
    fprintf(stderr, "This tool loads a voice, then speaks text or XML from a file or\n");
    fprintf(stderr, "standard input.\n\n");
    fprintf(stderr, "Usage: %s [Options] voice_file license_file [input_file]\n", name);
    fprintf(stderr, "Options:\n"); 
    fprintf(stderr, " -h\t\t  Display help\n");
    fprintf(stderr, " -o output_file\t  Output audio to wave file\n");
    fflush(stderr);
    exit(0);
}

int main(int argc, char * argv[]){

    CPRCEN_engine * eng;
    CPRCEN_wav * sound;
    CPRC_sc_player * player = NULL;
    CPRC_sc_audio * buf = NULL;

    char * voice_file = NULL;
    char * license_file = NULL;
    char * text_file = NULL;
    char * file_out = NULL;
    FILE * text_fp;
    char text_data[MAX_FILE_SIZE], ch;
    int arg, i, j;

    /* Processing arguments */
    arg = 0;
    for (i = 1; i < argc; i++) {
        /* Options */
	if (strcmp(argv[i], "-h") == 0) usage(argv[0]);
	else if (strcmp(argv[i], "-o") == 0) {
	    i++;
	    if (i < argc) {
		file_out = argv[i];
	    }
	    else usage(argv[0]);
	}
	/* Arguments */
	else {
	    switch(arg) {
	    case 0:
		voice_file = argv[i];
		break;
	    case 1:
		license_file = argv[i];
		break;
	    case 2:
		text_file = argv[i];
		break;
	    default:
		fprintf(stderr, "ERROR: unable to process argument '%s'\n", argv[i]);
		usage(argv[0]);
	    }
	    arg++;
	}
    }
    if (arg < 2 || arg > 4) usage(argv[0]);

    /* Load the voice */
    eng = CPRCEN_engine_load(license_file, voice_file);
    if (!eng) {
	fprintf(stderr, "ERROR: unable to load voice file '%s', exiting\n", voice_file);
	exit(-1);
    }
    fprintf(stderr, "INFO: voice loaded\n");

    /* Load the text to generate */
    if (text_file == NULL)
	text_fp = stdin;
    else {
	text_fp = fopen(text_file, "rb");
	if (!text_fp) {
	    fprintf(stderr, "ERROR: unable to open text file, '%s', exiting\n", text_file);
	    exit(-1);
	}
    }

    /* Construct string to send for synthesis */
    for (j = 0; (ch = fgetc(text_fp)) != EOF && j < MAX_FILE_SIZE; j++) text_data[j] = ch;
    text_data[j] = '\0';
    if(j == MAX_FILE_SIZE && ch != EOF)	
	fprintf(stderr,  "WARNING: input file exceeds maximum permitted file size, and will be truncated\n");

    /* Generate synthesis */
    fprintf(stderr, "INFO: read input text '%s'\n", text_data);
    if (file_out)
	/* Speak to file, writes a wav file */
	CPRCEN_engine_speak_to_file(eng, text_data, file_out);
    else {
	/* Synthesise the audio to an buffer */
	sound = CPRCEN_engine_speak(eng, text_data);
	/* Create an audio player */
	player = CPRC_sc_player_new(sound->sample_rate);
	/* Create an audio playback buffer.  Note that the
	   tts_callback.c example uses a 'disposable' buffer, which is
	   cleaned up by the player after use.  Here the buffer is
	   retained for monitoring. */
	buf = CPRC_sc_audio_short(sound->wavdata, sound->size);
	/* Cue the audio for playback */
	CPRC_sc_audio_cue(player, buf);
    }
    fprintf(stderr, "INFO: synthesis complete\n");

    /* If we're playing audio, wait for completion before quitting */
    if (!file_out) {
	while (CPRC_sc_audio_status(buf) != CPRC_SC_PLAYED)
	    CPRC_sc_sleep_msecs(50);
	CPRC_sc_audio_delete(buf);
	CPRC_sc_player_delete(player);
    }

    /* Clean up. The engine deletion function cleans up all loaded
       voices and closes any open channels */
    CPRCEN_engine_delete(eng);
    fclose(text_fp);

    return 0;
}
