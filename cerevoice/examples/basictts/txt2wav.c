/* Copyright (c) 2010-2011 Cereproc Ltd. */

/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the */
/* "Software"), to deal in the Software without restriction, including */
/* without limitation the rights to use, copy, modify, merge, publish, */
/* distribute, sublicense, and/or sell copies of the Software, and to */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions: */

/* The above copyright notice and this permission notice shall be */
/* included in all copies or substantial portions of the Software. */

/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE */
/* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION */
/* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION */
/* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cerevoice_eng.h>

#define MAX_READ 100000

void usage(char * name) {
    fprintf(stderr, "txt2wav - sample TTS program using the CereVoice Engine API\n\n");
    fprintf(stderr, "This tool loads a voice, then speaks a text or XML file to a wav file.\n\n");
    fprintf(stderr, "Usage: %s voice_file license_file input_file output_file\n", name);
    fflush(stderr);
    exit(0);
}

int main(int argc, char * argv[]){

    CPRCEN_engine * eng;
    CPRCEN_channel_handle chan;

    char * voice_file = NULL;
    char * license_file = NULL;
    char * text_file = NULL;
    char * out_file = NULL;
    char text_buffer[MAX_READ];
    char * retc;
    FILE * text_fp;
    int arg, i, ret;

    /* Processing arguments */
    arg = 0;
    for (i = 1; i < argc; i++) {
        /* Options */
	if (strcmp(argv[i], "-h") == 0) usage(argv[0]);
	/* Arguments */
	else {
	    switch(arg) {
	    case 0:
		voice_file = argv[i];
		break;
	    case 1:
		license_file = argv[i];
		break;
	    case 2:
		text_file = argv[i];
		break;
	    case 3:
		out_file = argv[i];
		break;
	    default:
		fprintf(stderr, "ERROR: unable to process argument %s\n", argv[i]);
		usage(argv[0]);
	    }
	    arg++;
	}
    }
    if (arg != 4) usage(argv[0]);

    /* Create a empty engine object.  The engine maintains the list of
       loaded voices and makes them available to synthesis channels. */
    eng = CPRCEN_engine_new();
    /* Configurable load function:
       CPRC_VOICE_LOAD - load all data to RAM (fastest)
       CPRC_VOICE_LOAD_EMB_AUDIO - audio database on disk (good
       performance and relatively low footprint)
       CPRC_VOICE_LOAD_EMB - more data structures on disk (slowest, 
       lowest RAM footprint)
     */
    ret = CPRCEN_engine_load_voice(eng, license_file, NULL, voice_file,
                            /* CPRC_VOICE_LOAD); */
                            /* CPRC_VOICE_LOAD_EMB); */
                            CPRC_VOICE_LOAD_EMB_AUDIO);
    if (!ret) {
	fprintf(stderr, "ERROR: unable to load voice file '%s', exiting\n", voice_file);
	CPRCEN_engine_delete(eng);
	exit(-1);
    }
    fprintf(stderr, "INFO: voice loaded\n");

    /* Set up a synthesis channel, and configure it for audio output
       to a file. */
    chan = CPRCEN_engine_open_default_channel(eng);
    CPRCEN_engine_channel_to_file(eng, chan, out_file, CPRCEN_RIFF);

    /* Load the text to generate */
    text_fp = fopen(text_file, "rb");
    if (!text_fp) {
	fprintf(stderr, "ERROR: unable to open text file: '%s', exiting\n", text_file);
	CPRCEN_engine_delete(eng);
	exit(-1);
    }

    /* Synthesise input line-by-line */
    while (!feof(text_fp)) {
	retc = fgets(text_buffer, MAX_READ, text_fp);
	if (!retc) break;
	fprintf(stderr, "INFO: text read '%s'\n", text_buffer);
	/* Synthesise the text buffer - the final argument is 'flush'.
	   Do not flush the buffer until all the input is sent.
	 */
	CPRCEN_engine_channel_speak(eng, chan, text_buffer, strlen(text_buffer), 0);
    }
    /* Finished processing, flush the buffer with empty input */
    CPRCEN_engine_channel_speak(eng, chan, "", 0, 1);

    fprintf(stderr, "INFO: synthesis done\n");

    /* Clean up. The engine deletion function cleans up all loaded
       voices and open channels */
    CPRCEN_engine_delete(eng);
    fclose(text_fp);

    return 0;
}
