var cerevoice__eng__simp_8h =
[
    [ "CPRCEN_wav", "struct_c_p_r_c_e_n__wav.html", "struct_c_p_r_c_e_n__wav" ],
    [ "CPRCEN_engine", "cerevoice__eng__simp_8h.html#a917a6f2532dfd4c46fb2cb28a403f180", null ],
    [ "CPRCEN_wav", "cerevoice__eng__simp_8h.html#a5d8a0d44af094f2dd6c312f698430c26", null ],
    [ "CPRCEN_engine_load", "cerevoice__eng__simp_8h.html#a4093b5e43899b81484dcf24e5fc9e3f9", null ],
    [ "CPRCEN_engine_load_config", "cerevoice__eng__simp_8h.html#aae12087b0ca05675ebfcaff9ea4ce339", null ],
    [ "CPRCEN_engine_delete", "cerevoice__eng__simp_8h.html#a67a959c55e799a6e1cafb90dfb39153a", null ],
    [ "CPRCEN_engine_speak", "cerevoice__eng__simp_8h.html#a9ff5e11acf7c58b7f6ed00f7b93c500d", null ],
    [ "CPRCEN_engine_speak_to_file", "cerevoice__eng__simp_8h.html#afde02103ae8ea2336d4437d7ab15ed68", null ],
    [ "CPRCEN_engine_clear", "cerevoice__eng__simp_8h.html#aebcd65ffdb00a5b7e3d78b8451d1674d", null ]
];