import os
import sys

# Add CereVoice Engine to the path
cur_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(cur_dir + '/cerevoice/cerevoice_eng/pylib')

import cerevoice_eng


def text_to_wav(msg, on_disk=False, dest_file='output.wav', voice='Heather'):
    """
    Convert text message to wav file.

    NOTE:
        - voice file(cerevoice.voice) should be '/home/pi/tts/cerevoice/cerevoice.voice'
        - License file should be '/home/pi/tts/cerevoice/license.lic'

    :param voice: Voice type, 'Heather' or 'Ada'
    :param dest_file:
    :param msg:
    :param on_disk: save data to RAM or with audio and indexes on disk.
    :return: /home/pi/tts/output.wav
    """
    print ' *** TTS : ', msg
    # Create an engine
    engine = cerevoice_eng.CPRCEN_engine_new()

    # Set the loading mode - all data to RAM or with audio and indexes on disk
    loadmode = cerevoice_eng.CPRC_VOICE_LOAD
    if on_disk:
        loadmode = cerevoice_eng.CPRC_VOICE_LOAD_EMB

    # Load the voice
    if voice == 'Heather':
        voice_path = cur_dir + '/cerevoice/cerevoice.voice'
    elif voice == 'Ada':
        voice_path = cur_dir + '/cerevoice/cerevoice_ada_3.2.0_48k.voice'
    else:
        print 'Unknown voice name'
        return None
    
    ret = cerevoice_eng.CPRCEN_engine_load_voice(engine, cur_dir + '/cerevoice/license.lic', "",
                                                 voice_path, loadmode)
    if not ret:
        print("ERROR: could not load the voice, check license integrity\n")
        return False
    # Get some information about the first loaded voice (index 0)
    name = cerevoice_eng.CPRCEN_engine_get_voice_info(engine, 0, "VOICE_NAME")
    srate = cerevoice_eng.CPRCEN_engine_get_voice_info(engine, 0, "SAMPLE_RATE")
    print("INFO: voice name is '%s', sample rate '%s'\n" % (name, srate))

    wavout = cur_dir + "/" + dest_file
    cerevoice_eng.CPRCEN_engine_speak_to_file(engine, msg, wavout)
    print("INFO: wrote wav file '%s'\n" % wavout)

    # Clean up
    cerevoice_eng.CPRCEN_engine_delete(engine)

if __name__ == '__main__':
    text_to_wav('Hi, This is Yuri.')
