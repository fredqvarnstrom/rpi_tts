## Preparing Raspberry Pi.

1. Expand file system with `sudo raspi-config` and reboot.
2. Run `sudo raspi-config` and check if it has the option `advanced options -> serial`. 
    If it has, set it to disabled

## Prepare license for Cerevoice TTS SDK.

- Copy `licence.lic` to `/home/pi/tts/cerevoice`.
- Copy `cerevoice.voice` to `/home/pi/tts/cerevoice`.


## AT commands for Cell Broadcasting in SIM800/900:

Seems like this is working:
        
    AT+CMGF=1
    AT+CNMI=2,0,2,0,0
    AT+CSCB=0,"4370","0-9"
    
Before this, refer p.114 of datasheet.
 
or 

    AT+CNMI=1,0,2,0,1
    AT+CSCB=0,"4370","0-15"
    

## Play sound on Raspberry Pi Zero with pHAT.

- Adding pHAT card.

        sudo nano /boot/config.txt
        
    Add line:
        
        dtoverlay=hifiberry-dac
    
    And comment line:
        
        #dtparam=audio=on
    
    And reboot.

- Disable bcm audio
  
        sudo nano /etc/modules

    This file, in contast to the blacklist, lists modules which we do want to load. We're going to remove the default sound driver with a comment, so change the line:

        snd_bcm2835
    To:

        #snd_bcm2835

- Set default audio.
    
    Type `aplay -l` to check sound card is correctly installed.
    
    You should see something like this:
        
        card 0: sndrpihifiberry [snd_rpi_hifiberry_dac], device 0: HifiBerry DAC HiFi pcm5102a-hifi-0 []
        Subdevices: 1/1
        Subdevice #0: subdevice #0
    
    This means pHAT card is detected as `card 0`
    
        sudo nano /etc/asound.conf
    
    And add this:
        
        pcm.!default  {
            type hw
            card 0
            device 0
        }
        ctl.!default {
            type hw
            card 0
            device 0
        }
    
- Play sound
    
        aplay -D sysdefault:CARD=0 output.wav

## Reduce booting time of Raspberry Pi. (Not tested...)

- Remove desktop related programmes and services from a stock Raspbian install. 
        
        sudo apt-get -y purge libx11-6 libgtk-3-common xkb-data lxde-icon-theme raspberrypi-artwork penguinspuzzle
        sudo apt-get -y install libfreetype6
        
- Using `systemd`

        sudo nano /etc/systemd/system/samplerbox.service

    Add this and save & exit.

        [Unit]
        Description=Starts SamplerBox
        DefaultDependencies=false            # Very important! Without this line, the service 
                                             # would wait until networking.service
                                             # has finished initialization. This could add 10 
                                             # more seconds because of DHCP, IP attribution, etc.
        
        [Service]
        Type=simple
        ExecStart=/root/SamplerBox/samplerbox.sh
        WorkingDirectory=/root/SamplerBox/
        
        [Install]
        WantedBy=local-fs.target
    
    After rebooting, type this:
    
        systemctl enable samplerbox

    And reboot.
    
## Enable auto-starting
    
We are assuming that all source files are located at `/home/pi/tts`.

    mkdir /home/pi/tts/logs
    nano /home/pi/tts.sh

Add these to the file, and save & exit.
    
    #!/bin/bash
    dt=$(date '+%Y-%m-%d_%H_%M_%S');
    cd /home/pi/tts
    /usr/bin/python /home/pi/tts/tts.py > /home/pi/tts/logs/"$dt".log 2>&1

Make it executable
    
    sudo chmod +x /home/pi/tts.sh

Open `/etc/rc.local` to start this automatically.
    
    sudo nano /etc/rc.local

Add this before `exit 0`
    
    (/bin/bash /home/pi/tts.sh)&

After all, our python script will run automatically and its all output messages will be saved in 
`/home/pi/tts/logs` directory with date stamp. Sweet!

## Change voice language.
    
To change the language of TTS voice, we just need to change a environment variable.

To change the voice to Dutch Ada:
    
    sudo nano /home/pi/tts/sentence.py

At the first part of this file, you could see something like `voice = 'Heather'`.

In order to change the voice, we just need to change it to:
    
    voice = 'Ada'

To change back to the original:
    
    voice = 'Heather'
    
    

