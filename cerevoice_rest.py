"""
To see all voices, please visit https://www.cereproc.com/en/products/cloud

"""
from suds.client import Client
import urllib

accountId = "57e2807827499"
password = "E2WNbfCdid"
soap_client = Client("https://cerevoice.com/soap/soap_1_1.php?WSDL")


def download(text, voice="Katherine", audio_format="wav", file_name=""):
    """
    Downloads a synthesised voice audio file and saves to the given filename.
    If no filename is provided, the following format is used: <voice>_<text>.<format>
    :param text: Message to synthesis.
    :param voice: Voice to be used for synthesis.
    :param audio_format: Format of audio file.
    :param file_name: Output filename.
    :return: Download state of Boolean type
    """
    reply = soap_client.service.speakExtended(accountId, password, voice, text, audio_format, 22050, False, False)

    if file_name == "":
        file_name = "%s_%s.%s" % (voice, text, audio_format)

    if reply.resultCode != 1:
        print "Unable to download file:", reply.resultDescription
        return False
    else:
        urllib.urlretrieve(reply.fileUrl, file_name)
        print "Retrieved synthesised voice for '%s'" % text
        return True


if __name__ == '__main__':
    download("Nice to meet you.")
